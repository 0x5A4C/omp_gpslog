from setuptools import setup

setup(
   name='foo',
   version='1.0',
   description='A useful module',
   author='Man Foo',
   author_email='foomail@foo.com',
   packages=['omp_gpslog'],  #same as name
   install_requires=['staticmap' , 'gpxpy' , 'rst' , 'rtree' , 'sphinx' , 'sphinx-corlab-theme'], #external packages as dependencies
)
