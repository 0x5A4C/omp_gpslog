#!/usr/bin/env python

from os.path import isfile, join
from os import listdir
import logging
from pypifi.pipeline import Pipeline
from gpsLogTitle import *

logger = logging.getLogger("gpsLogger")
logger.setLevel(logging.DEBUG)

# create file handler which logs info messages
fh = logging.FileHandler('gpsLogGenerator.log', 'w', 'utf-8')
fh.setLevel(logging.DEBUG)

# create console handler with a debug log level
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

# creating a formatter
formatter = logging.Formatter('- %(name)-40s - %(levelname)-8s: %(message)s')

# setting handler format
fh.setFormatter(formatter)
ch.setFormatter(formatter)

# add the handlers to the logger
logger.addHandler(fh)
logger.addHandler(ch)


if __name__ == '__main__':
    logger.info('--------')

    pipe = Pipeline()
    GpsLogCleanOutFilter
    pipe.connect(GpsLogCleanOutFilter())
    pipe.connect(GpsLogGetFilesFilter())
    pipe.connect(GpsLogDocFilter())
    pipe.connect(GpsLogStatisticFilter())
    pipe.connect(GpsLogMapFilter())
    pipe.connect(GpsLogTripPointsFilter())
    # pipe.connect(GpsLogStaticMapFilter())
    # pipe.connect(GpsLogGalleryFilter())
    pipe.connect(GpsLogTitleFilter())
    pipe.connect(GpsLogGenerateFileFilter())
    pipe.connect(GpsLogIndexFilter())
    pipe.connect(GpsLogRubSphinxFilter())

    gpsLogMessage = GpsLogMessage("")
    gpsLogMessage.path = "./gpx/"

    # onlyfiles = [f for f in listdir(
    #     gpsLogMessage.path) if isfile(join(gpsLogMessage.path, f))]
    # for gpx_file in onlyfiles:
    #     if gpx_file.endswith('.gpx'):
    #         logger.debug(gpx_file)
    #         gpsLogMessage.gpsFileName = os.path.join("./gpx/", gpx_file)
    #         pipe.execute(gpsLogMessage)

    pipe.execute(gpsLogMessage)
