===================
Title of the report
===================


Statistic
---------

.. list-table:: Some stat
    :header-rows: 1

    * -  Par
      -  Val
    * -  Length 2D
      -  18.30 km
    * -  Length 3D
      -  18.60 km
    * -  Moving time
      -  03:44:37            
    * -  Stopped time
      -  01:10:00            
    * -  Max speed
      -  7.70 km/h
    * -  Total uphill
      -  780.60
    * -  Total downhill
      -  935.60
    * -  Started
      -  10-26-2014 10:30:28
    * -  Ended
      -  10-26-2014 15:32:49

Map and profile
---------------


.. raw:: html

    <style> html, body, #map, #elevation-div { height: 100%; width: 100%; padding: 0; margin: 0; } #map { height: 75%; } #elevation-div {       height: 25%; font: 12px/1.5 "Helvetica Neue", Arial, Helvetica, sans-serif; } </style>
    <!-- Leaflet (JS/CSS) -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.2/dist/leaflet.css" />
    <script src="https://unpkg.com/leaflet@1.3.2/dist/leaflet.js"></script>
    <!-- D3.js -->
    <script src="https://unpkg.com/d3@4.13.0/build/d3.min.js" charset="utf-8"></script>
    <!-- leaflet-gpx -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet-gpx/1.4.0/gpx.js"></script>
    <!-- leaflet-elevation -->
    <link rel="stylesheet" href="https://unpkg.com/@raruto/leaflet-elevation/dist/leaflet-elevation.css" />
    <script src="https://unpkg.com/@raruto/leaflet-elevation/dist/leaflet-elevation.js"></script>

    <div style="width:800px; height:800px" id="map"></div>
    <div id="elevation-div"></div>

    <script>
      var opts = {
        map: {
          center: [41.4583, 12.7059],
          zoom: 5,
          markerZoomAnimation: false,
          zoomControl: false,
        },
        zoomControl: {
          position: 'topleft',
        },
        otmLayer: {
          url: 'https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png',
          options: {
            attribution: 'Map data: &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)',
            /*subdomains:"1234"*/
          },
        },
        elevationControl: {
          data: "http://127.0.0.1:8000/_static/gpx/2014-10-26_11-30-28.gpx",
          options: {
            position: "topleft",
            theme: "magenta-theme", //default: lime-theme
            useHeightIndicator: true, //if false a marker is drawn at map position
            collapsed: false, //collapsed mode, show chart on click or mouseover
            detachedView: true, //if false the chart is drawn within map container
            elevationDiv: "#elevation-div", // if (detached), the elevation chart container
          },
        },
        layersControl: {
          options: {
            collapsed: false,
          },
        },
      };

      var map = new L.Map('map', opts.map);

      var baseLayers = {};
      baseLayers.OTM = new L.TileLayer(opts.otmLayer.url, opts.otmLayer.options);

      var controlZoom = new L.Control.Zoom(opts.zoomControl);
      var controlElevation = L.control.elevation(opts.elevationControl.options);
      var controlLayer = L.control.layers(baseLayers, null, opts.layersControl.options);

      controlZoom.addTo(map);
      controlLayer.addTo(map);
      controlElevation.addTo(map); // attach elevation chart to map

      controlElevation.loadData(opts.elevationControl.data); // url or plain gpx/geojson data

      map.addLayer(baseLayers.OTM);
    </script></div>




Trip points
-----------

Przełęcz Jugowska, Rymarz, Słoneczna, Kalenica, Żmij, Bielawska Polana, Popielak, Wigancicka Polana, Dzik, Średniak, Przełęcz Woliborska, Kobylec, Zległe, Szeroka, Przełęcz pod Szeroką, Malinowa, Przełęcz Pod Gołębią, Gołębia, Gąsiorek, Chochoł Wielki, Chochoł Mały, Przełęcz Srebrna, Michałków, Ostróg, 

