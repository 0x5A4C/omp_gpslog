#!/usr/bin/env python


# from staticmap import StaticMap, Line #https://github.com/trailbehind/staticmap
import math as mod_math
import gpxpy

import os

from pypifi.pipeline import Pipeline
from pypifi.filter import Filter
from pypifi.message import Message
from pypifi.common import CommonLogging
from rst import rst
from string import Template
from geoNames.geoNamesAdapter import *
from geoNames.geoNames import *
from shutil import copyfile
from os import listdir
from os.path import isfile, join

from pypifi.common import CommonLogging

from pprint import pprint

import shutil
import subprocess


class Report(CommonLogging):
    def __init__(self):
        CommonLogging.__init__(self)


class GpsLogMessage(Message):
    def __init__(self, gpsFileName):
        Message.__init__(self)
        self.gpsFileName = gpsFileName
        self.parent = None
        self.sub_messeges = []
        self.path = ""


class GpsLogCleanOutFilter(Filter):
    def __init__(self):
        Filter.__init__(self)

    def process(self, message):
        self.logger.debug("process")

        out_path = os.path.dirname(os.path.abspath(__file__))
        out_src_path = os.path.join(out_path, "out", "_source")
        out_src_sta_path = os.path.join(out_src_path, "_static")
        out_src_gpx_path = os.path.join(out_src_path, "gpx")

        shutil.rmtree(out_src_sta_path, ignore_errors=True)
        shutil.rmtree(out_src_gpx_path, ignore_errors=True)


class GpsLogRubSphinxFilter(Filter):
    def __init__(self):
        Filter.__init__(self)

    def process(self, message):
        self.logger.debug("process")

        os.chdir("./out/")
        os.system("make html")
        # subprocess.run(["make", "html"])


class GpsLogGetFilesFilter(Filter):
    def __init__(self):
        Filter.__init__(self)

    def process(self, message):
        self.logger.debug("process")

        message.gpx_rep = {}
        self.logger.debug("script.path: {0}".format(os.getcwd()))
        self.logger.debug("message.path: {0}".format(os.path.dirname(__file__)))        
        self.logger.debug("message.path: {0}".format(message.path))

        for f in listdir(message.path):
            self.logger.debug("el. of message.path: {0}".format(f))
            if isfile(join(message.path, f)):
                message.gpx_rep[f] = Report()
            else:
                gpsLogMessage = GpsLogMessage("")
                # gpsLogMessage.path = os.path.join("./gpx/", f)
                gpsLogMessage.path = os.path.join(message.path, f)
                gpsLogMessage.parent = message
                message.sub_messeges.append(gpsLogMessage)
                sub_pipe = Pipeline()
                sub_pipe.connect(GpsLogGetFilesFilter())
                sub_pipe.connect(GpsLogDocFilter())
                sub_pipe.connect(GpsLogStatisticFilter())
                sub_pipe.connect(GpsLogMapFilter())
                sub_pipe.connect(GpsLogTripPointsFilter())
                # sub_pipe.connect(GpsLogStaticMapFilter())
                # sub_pipe.connect(GpsLogGalleryFilter())
                sub_pipe.connect(GpsLogTitleFilter())
                sub_pipe.connect(GpsLogGenerateFileFilter())
                sub_pipe.connect(GpsLogIndexFilter())
                sub_pipe.execute(gpsLogMessage)


class GpsLogDocFilter(Filter):
    def __init__(self):
        Filter.__init__(self)

    def process(self, message):
        self.logger.debug("process")

        for key, value in message.gpx_rep.items():
            value.rst = rst.Document('Title of the report')
            value.gpx_report = None


# ------------------------------------------------------------------------------
class geoSectionGpxSummaryCommon(dict):
    gpxSegment = None
    indentation = "\t\t"
    string = ""

    def __init__(self, *arg, **kw):
        super(geoSectionGpxSummaryCommon, self).__init__(*arg, **kw)

        self.gpxSegment = None
        self.indentation = "\t\t"
        self.string = ""

    def processGpx(self):
        length_2d = self.gpxSegment.length_2d()
        length_3d = self.gpxSegment.length_3d()
        self["Length 2D"] = length_2d / 1000
        self["Length 3D"] = length_3d / 1000

        moving_time, stopped_time, moving_distance, stopped_distance, max_speed = self.gpxSegment.get_moving_data()
        self["Moving time"] = self.formatTime(moving_time)
        self["Stopped time"] = self.formatTime(stopped_time)
        self["Stopped distance"] = stopped_distance
        self["Max speed"] = max_speed

        uphill, downhill = self.gpxSegment.get_uphill_downhill()
        self["Total uphill"] = uphill
        self["Total downhill"] = downhill

        start_time, end_time = self.gpxSegment.get_time_bounds()
        self["Started"] = start_time
        self["Ended"] = end_time

        points_no = len(list(self.gpxSegment.walk(only_points=True)))
        self["Points"] = points_no

        distances = []
        previous_point = None
        for point in self.gpxSegment.walk(only_points=True):
            if previous_point:
                distance = point.distance_2d(previous_point)
                distances.append(distance)
            previous_point = point
        self["Avg distance between points"] = sum(
            distances) / len(list(self.gpxSegment.walk()))

    def __str__(self):

        self.string += "{0}{1:<20s}{2}{3:.2f} km\n".format(
            self.indentation, "Length 2D:", "", self["Length 2D"])
        self.string = self.string + "{0}{1:<20s}{2}{3:.2f} km\n".format(
            self.indentation, "Length 3D:", "", self["Length 3D"])
        self.string += "{0}{1:<20s}{2}{3}\n".format(
            self.indentation, "Moving time:", "", self["Moving time"])
        self.string += "{0}{1:<20s}{2}{3}\n".format(
            self.indentation, "Stopped time:", "", self["Stopped time"])
        self.string += "{0}{1:<20s}{2}{3:.2f} m/s = {4:.2f} km/h\n".format(self.indentation, "Max speed:", "",
                                                                           self["Max speed"],
                                                                           self["Max speed"] * 60. ** 2 / 1000. if self[
                                                                               "Max speed"] else 0)

        self.string += "{0}{1:<20s}{2}{3:.0f} m\n".format(
            self.indentation, "Total uphill:", "", self["Total uphill"])
        self.string += "{0}{1:<20s}{2}{3:.0f} m\n".format(
            self.indentation, "Total downhill:", "", self["Total downhill"])

        self.string += "{0}{1:<20s}{2}{3}\n".format(
            self.indentation, "Started:", "", self["Started"])
        self.string += "{0}{1:<20s}{2}{3}\r\n".format(
            self.indentation, "Ended:", "", self["Ended"])

        # string += "{0}{1:<20s}{2}{3:.2f} m\n\n".format(self.indentation, "Avg distance between points:", "", self["Avg distance between points"])

        return self.string

    def formatTime(self, time_s):
        if not time_s:
            return 'n/a'
        minutes = mod_math.floor(time_s / 60.)
        hours = mod_math.floor(minutes / 60.)

        return '%s:%s:%s' % (str(int(hours)).zfill(2), str(int(minutes % 60)).zfill(2), str(int(time_s % 60)).zfill(2))


class geoSectionGpxSegmentSummary(geoSectionGpxSummaryCommon):
    def __init__(self, gpxSegment, parentGpxSummary, *arg, **kw):
        geoSectionGpxSummaryCommon.__init__(self, *arg, **kw)

        self.gpxSegment = gpxSegment

        try:
            self.processGpx()
            if parentGpxSummary != None:
                parentGpxSummary["Segments"].append(self)

        except Exception as e:
            print("Error parsing gpxSegment: {0}.".format(e))


class geoSectionGpxDataSummary(geoSectionGpxSummaryCommon):
    def __init__(self, gpxData, *arg, **kw):
        # super(geoSectionGpxDataSummary, self).__init__(*arg, **kw)

        try:
            self.gpxSegment = gpxData

            self.processGpx()

            for track in self.gpxSegment.tracks:
                for segment in track.segments:
                    self["Segments"] = []
                    geoSectionGpxSegmentSummary(
                        gpxSegment=segment, parentGpxSummary=self)

        except Exception as e:
            print("Error opening or parsing: {0}.".format(e))

    def __str__(self):
        outStr = ""

        outStr += geoSectionGpxSummaryCommon.__str__(self)

        for gpxSegmentSummary in self["Segments"]:
            gpxSegmentSummary.indentation = gpxSegmentSummary.indentation + \
                gpxSegmentSummary.indentation
            outStr += "\n\n"
            outStr += gpxSegmentSummary.__str__()

        return outStr

# ------------------------------------------------------------------------------


class GpsLogStatisticFilter(Filter):
    def __init__(self):
        Filter.__init__(self)

    def process(self, message):
        self.logger.debug("process")

        for key, value in message.gpx_rep.items():
            self.logger.debug(os.path.join(message.path, key))
            gpx_file = open(os.path.join(message.path, key), 'r')
            value.parsed_gpx = gpxpy.parse(gpx_file)
            summary = geoSectionGpxDataSummary(value.parsed_gpx)
            value.summary = summary

            tbl = rst.Table('', ['Par', 'Val'])
            tbl.add_item(
                ('Length 2D', "{0:.2f} km".format(summary["Length 2D"])))
            tbl.add_item(
                ('Length 3D', "{0:.2f} km".format(summary["Length 3D"])))
            tbl.add_item(
                ('Moving time', "{0:<20s}".format(summary["Moving time"])))
            tbl.add_item(
                ('Stopped time', "{0:<20s}".format(summary["Stopped time"])))
            tbl.add_item(('Max speed', "{0:.2f} km/h".format(summary["Max speed"] * 60. ** 2 / 1000. if summary[
                "Max speed"] else 0)))
            tbl.add_item(
                ('Total uphill', "{0:.2f}".format(summary["Total uphill"])))
            tbl.add_item(
                ('Total downhill', "{0:.2f}".format(summary["Total downhill"])))
            tbl.add_item(('Started', "{0}".format(
                summary["Started"].strftime("%Y-%m-%d %H:%M:%S"))))
            tbl.add_item(('Ended', "{0}".format(
                summary["Ended"].strftime("%Y-%m-%d %H:%M:%S"))))

            sec = rst.Section('Stats', 2)
            value.rst.add_child(sec)

            value.rst.add_child(tbl)


class GpsLogMapFilter(Filter):
    def __init__(self):
        Filter.__init__(self)

    def process(self, message):
        self.logger.debug("process")

        for key, value in message.gpx_rep.items():

            sec = rst.Section('Map and profile', 2)
            value.rst.add_child(sec)

            para = rst.Paragraph(
                self.get_trip_log_leaflet_map(os.path.join("out/_source/_static/gpx/",  os.path.basename(key))))
            value.rst.add_child(para)

    def get_trip_log_leaflet_map(self, gpx):
        map_html = ""

        # template = open('leaflet_map_tm.tmpl', 'r').read()
        template = open('leaflet_map_os.tmpl', 'r').read()
        map_html = Template(template).substitute(
            {'gpx': os.path.join('../../_static', 'gpx', os.path.basename(gpx))})

        return map_html


class GpsLogTripPointsFilter(Filter):
    def __init__(self):
        Filter.__init__(self)

    def process(self, message):
        self.logger.debug("process")

        _geoNamesAdapterCsvFile_geoNames = geoNamesAdapterCsvFile_geoNames(
            "./geoNames/PL.txt")
        _geoNamesAdapterCsvFile_geoPortal = geoNamesAdapterCsvFile_geoPortal(
            "./geoNames/obiekty_fizjograficzne.csv")

        for key, value in message.gpx_rep.items():
            self.logger.debug(os.path.join(message.path, key))
            points = value.parsed_gpx.tracks[0].segments[0].points
            value.trip_points = geoNames(
                points, _geoNamesAdapterCsvFile_geoNames)

            geo_name_gpx_str = ""
            for i, geoNameGpx in enumerate(value.trip_points):
                geo_name_gpx_str += geoNameGpx["name"] + ", "

            sec = rst.Section('Trip points', 2)
            value.rst.add_child(sec)

            para = rst.Paragraph(geo_name_gpx_str)
            value.rst.add_child(para)


class GpsLogTitleFilter(Filter):
    def __init__(self):
        Filter.__init__(self)

    def process(self, message):
        self.logger.debug("process")

        for key, value in message.gpx_rep.items():
            if hasattr(value.parsed_gpx.tracks[0], "name"):
                value.rst.title = value.parsed_gpx.tracks[0].name
            else:
                value.rst.title = value.trip_points[0]["name"] + " - " + value.trip_points[-1]["name"] + " " + value.summary["Started"].strftime(
                    "%Y-%m-%d - ") + value.summary["Ended"].strftime("%Y-%m-%d ")


# class GpsLogStaticMapFilter(Filter):
#     def __init__(self):
#         Filter.__init__(self)

#     def process(self, message):
#         self.logger.debug("process")

#         m = StaticMap(200, 200, 80)

#         coordinates = [[12.422, 45.427], [13.749, 44.885]]
#         line_outline = Line(coordinates, 'white', 6)
#         line = Line(coordinates, '#D2322D', 4)

#         m.add_line(line_outline)
#         m.add_line(line)

#         image = m.render()
#         image.save('ferry.png')


# class GpsLogGalleryFilter(Filter):
#     def __init__(self):
#         Filter.__init__(self)

#     def process(self, message):
#         self.logger.debug("process")


class GpsLogGenerateFileFilter(Filter):
    def __init__(self):
        Filter.__init__(self)

    def process(self, message):
        self.logger.debug("process")

        out_path = os.path.dirname(os.path.abspath(__file__))
        directory = os.path.join(out_path, "out", "_source", message.path)
        if not os.path.exists(directory):
            os.makedirs(directory)

        for key, value in message.gpx_rep.items():
            self.logger.debug(os.path.join(message.path, key))
            # file_name = value.summary["Started"].strftime("%Y%m%d") + "_" + value.summary["Ended"].strftime(
            #     "%Y%m%d") + "_" + value.trip_points[0]["name"] + "_" + value.trip_points[-1]["name"] + ".rst"
            file_name = str(key) + ".rst"
            file_name = file_name.replace(" ", "")
            value.gpx_report = file_name

            file = open(os.path.join(directory, file_name), "w")
            file.write(value.rst.get_rst())
            file.close()

            if not os.path.exists("out/_source/_static/gpx/"):
                os.makedirs("out/_source/_static/gpx/")

            copyfile(join(message.path, key), os.path.join(
                "out/_source/_static/gpx/",  os.path.basename(key)))


class GpsLogIndexFilter(Filter):
    def __init__(self):
        Filter.__init__(self)

    def gen_toc_from_msg(self, toc, msg):

        msg.index_doc = rst.Document('Tracks')
        toc_ = rst.TocTree("---", 1)
        for key, rep in msg.gpx_rep.items():
            toc_.add_item(rep.gpx_report)
        msg.index_doc.add_child(toc_)

        for sub_msg in msg.sub_messeges:
            self.gen_toc_from_msg(toc_, sub_msg)

        file_name = os.path.join(msg.path, "index.rst")
        if toc is not None:
            toc.add_item(os.path.join(
                os.path.split(msg.path)[-1], "index.rst"))
            msg.index_doc.title = os.path.split(msg.path)[-1]

        out_path = os.path.dirname(os.path.abspath(__file__))
        directory = os.path.join(out_path, "out", "_source")
        if not os.path.exists(directory):
            os.makedirs(directory)
        file = open(os.path.join(directory, file_name), "w")
        file.write(msg.index_doc.get_rst())
        file.close()

    def process(self, message):
        self.logger.debug("process")

        self.gen_toc_from_msg(None, message)
