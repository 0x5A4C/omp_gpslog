#!/usr/bin/env python

import logging


class Common(object):
    def __init__(self):
        pass


class CommonLogging(Common):
    def __init__(self):
        Common.__init__(self)
        self.logger = logging.getLogger("gpsLogger."+self.__class__.__name__)
        self.logger.info('creating an instance of Class')
