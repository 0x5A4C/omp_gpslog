#!/usr/bin/env python

from pypifi.common import CommonLogging


class Pipeline(CommonLogging):
    def __init__(self):
        CommonLogging.__init__(self)

        self.filters = []

    def connect(self, filter):
        self.filters.append(filter)
        return self

    def execute(self, message):
        for filter in self.filters:
            filter.process(message)
