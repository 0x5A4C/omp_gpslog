#!/usr/bin/env python

from pypifi.common import CommonLogging


class Filter(CommonLogging):
    """
        Base filter class which can take a message and alter the message appropriately.
        You define what happens with the message.
    """

    def __init__(self):
        CommonLogging.__init__(self)

    def process(self, message):
        self.logger.debug("process")
