#!/usr/bin/env python

from pypifi.common import CommonLogging


class Message(CommonLogging):
    """
        Base type of a message sent through the pipeline.
        Define some attributes and methods to form your message.

        I suggest you don't alter this class. You're are free to do so, of course. It's your own decision.
        Though, I suggest you create your own message type and let it inherit from this class.
    """

    def __init__(self):
        CommonLogging.__init__(self)
